package com.mycompany.practicasistemacentralizado;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Test {
    
     public static void main(String[] args) {
        Test t  = new Test();
        t.insertar(1, "PERA", 33.15);
        //t.actualizar(1, "PERA", 33.15);
        //t.eliminar(1);
    }
     
    public Connection getConexion() 
    {
        Connection conexion = null;
        String servidor = "localhost";
        String puerto = "5432";
        String baseDatos = "supermercado";
        String url = "jdbc:postgresql://"+servidor+":"+puerto+"/"+baseDatos;
        String usuario="postres";
        String password="1719817191";
        try{
            conexion = DriverManager.getConnection(url, usuario, password);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return conexion;
    }
    
     public void insertar(int codigo, String nombre, double precio) {
        Connection conexion = getConexion();
        //String sql = "INSERT INTO practicauno (codigo,nombre,precio) VALUES (2,'AZUCAR',12.56)";
        String sql = "insert into producto values (" + codigo + ",'" + nombre + "','" + precio + "')";
        try (Statement st = conexion.createStatement()) {
            //EL executeUpdate ES LA EJECUCIÓN DE LA SENTENCIA
            st.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
     public void actualizar(int codigo, String nombre, double precio) {
        Connection conexion = getConexion();
        String sql = "UPDATE practicauno SET " + "nombre='" + nombre + "'" + ",precio=" + precio + "WHERE codigo=" + codigo;
        try (Statement st = conexion.createStatement()) {
            st.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    public void eliminar(int codigo) {
        Connection conexion = getConexion();
        String sql = "DELETE FROM practicauno WHERE codigo = " + codigo;
        try (Statement st = conexion.createStatement()) 
        {
            st.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
